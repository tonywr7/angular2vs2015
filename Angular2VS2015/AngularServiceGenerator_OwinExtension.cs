﻿using Microsoft.AspNet.SignalR.Json;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using Microsoft.Owin.Extensions;
using System.Threading.Tasks;
using System.IO;
using AppFunc = System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>;
using System.Web.Http;
using System.Diagnostics;
using System.Reflection;

namespace Owin
{
  [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Owin", Justification = "The owin namespace is for consistentcy.")]
  public static class OwinExtensions
  {
    public static IAppBuilder AngularServiceJavascriptGenerator(this IAppBuilder builder)
    {
      return builder.Map("/services", map => {
        map.Use<AngularServiceGeneratorMiddleware>();
      });
    }

  }

  public class AngularServiceGeneratorMiddleware : OwinMiddleware
  {
    public AngularServiceGeneratorMiddleware(OwinMiddleware next) : base(next)
    {
    }

    private string headerOfService =
@"
""use strict"";
var __decorate = (this && this.__decorate) || function(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === ""object"" && typeof Reflect.decorate === ""function"") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c< 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
  var __metadata = (this && this.__metadata) || function(k, v) {
    if (typeof Reflect === ""object"" && typeof Reflect.metadata === ""function"") return Reflect.metadata(k, v);
};
var core_1 = require(""@angular/core"");
var http_1 = require(""@angular/http"");
var Rx_1 = require(""rxjs/Rx"");

";

    private string mainService =
@"var /*ServiceName*/Service = (function () {
    function /*ServiceName*/Service(http) {
        this.http = http;
    }
/*methods*/
    /*ServiceName*/Service.prototype.handleError = function(error)
    {
      return RX_1.Observable.throw (error.statusText);
    };
 return /*ServiceName*/Service;
}());
/*ServiceName*/Service = __decorate([
    core_1.Injectable(),
    __metadata(""design:paramtypes"", [http_1.Http])
], /*ServiceName*/Service);
exports./*ServiceName*/Service = /*ServiceName*/Service;


";

    public async override Task Invoke(IOwinContext context)
    {
      var message = string.Format("You have invokes the services url, with a specific request for the {0} service", context.Request.Path.Value);

      var outputScript = headerOfService; ;

      //get a list of all apicontroller classes
      var allApiControllers = AppDomain.CurrentDomain.GetAssemblies().SelectMany(assembly => assembly.GetTypes()).Where(type => type.IsSubclassOf(typeof(ApiController)));
      foreach(var apiController in allApiControllers)
      {
        var controllerName = GetModifiedControllerNameForJavascript(apiController.Name);
        foreach (var method in apiController.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly))
        {
          var parameters = "";
          foreach (var param in method.GetParameters())
          {
            if (parameters.Length > 0) parameters += ",";
            parameters += param.ParameterType.Name + " " + param.Name;
          }
          Debug.WriteLine("{0}.{1}({2})", apiController.Name, method.Name, parameters);
        }
        outputScript += mainService.Replace("/*ServiceName*/", controllerName);
      }

      //      var script =
      //@"var EventService = (function () {
      //    function EventService(http) {
      //        this.http = http;
      //    }
      //    EventService.prototype.getEvents = function () {
      //        return this.http.get(""/api/events/getEvents"").map(function (response) {
      //            return response.json();
      //    }).catch(this.handleError);
      //    };
      //    EventService.prototype.handleError = function(error)
      //    {
      //      return RX_1.Observable.throw (error.statusText);
      //    };
      // return EventService;
      //}());
      //";



      context.Response.ContentType = JsonUtility.JavaScriptMimeType;
      await context.Response.WriteAsync("alert('yello!');");
      //await context.Response.WriteAsync(string.Format("alert('{0}');",message));
      //await context.Response.WriteAsync(outputScript);
    }

    private static string GetModifiedControllerNameForJavascript(string controllerName)
    {
      if (controllerName.EndsWith("Controller"))
      {
        controllerName = controllerName.Substring(0, controllerName.Length - 10);
      }
      //if (controllerName.Length > 0) controllerName = controllerName.Substring(0, 1).ToLowerInvariant() + controllerName.Substring(1);
      return controllerName;
    }
  }

}