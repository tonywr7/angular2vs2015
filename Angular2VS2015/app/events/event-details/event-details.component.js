"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var event_service_1 = require("../shared/event.service");
var router_1 = require("@angular/router");
var EventDetailsComponent = (function () {
    function EventDetailsComponent(eventService, route) {
        this.eventService = eventService;
        this.route = route;
        this.filterBy = 'all';
        this.sortBy = 'votes';
    }
    EventDetailsComponent.prototype.ngOnInit = function () {
        //routing a component to itself needs a complete reset of the state. To achieve this, nemove the snapshot version of this.event below and add in the forEach version
        var _this = this;
        //the following was removed because we needed to move from a params foreach to a data foreach to support using a resolver for data retrieval
        //this.route.params.forEach((params: Params) => {
        //  // the following code was removed to move from a synchronous call to publisher/subscriber call
        //  //this.event = this.eventService.getEvent(+params['id'])
        //  //this.addMode = false;
        //  // the following was removed to move the code into a resolver, which will preload before running the page (it's an attibute in the router)
        //  //this.eventService.getEvent(+params['id']).subscribe((event: IEvent) => {
        //  //  this.event = event;
        //  //  this.addMode = false;
        //  //});
        //  //the following is needed only when the data doesn't change. We no longer want to subscribe to the parameters, we're better off subscribing to the data'
        //  //this.event = this.route.snapshot.data["event"];
        //  this.addMode = false;
        //})
        //this.event = this.eventService.getEvent(+this.route.snapshot.params['id'])
        this.route.data.forEach(function (data) {
            _this.event = data['event'];
            _this.addMode = false;
        });
    };
    EventDetailsComponent.prototype.addSession = function () {
        this.addMode = true;
    };
    EventDetailsComponent.prototype.saveNewSession = function (session) {
        var nextId = Math.max.apply(null, this.event.sessions.map(function (s) { return s.id; }));
        session.id = nextId + 1;
        this.event.sessions.push(session);
        //this.eventService.updateEvent(this.event)
        this.eventService.saveEvent(this.event).subscribe(function (event) { });
        this.addMode = false;
    };
    EventDetailsComponent.prototype.cancelAddSession = function () {
        this.addMode = false;
    };
    return EventDetailsComponent;
}());
EventDetailsComponent = __decorate([
    core_1.Component({
        templateUrl: '/app/events/event-details/event-details.component.html',
        styles: ["\n    .container { padding-left:20px; padding-right:20px; }\n    .event-image { height: 100px; }\n    a {cursor: pointer}\n  "]
    }),
    __metadata("design:paramtypes", [event_service_1.EventService, router_1.ActivatedRoute])
], EventDetailsComponent);
exports.EventDetailsComponent = EventDetailsComponent;
//# sourceMappingURL=event-details.component.js.map