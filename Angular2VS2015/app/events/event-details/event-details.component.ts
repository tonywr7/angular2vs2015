import { Component } from '@angular/core'
import { EventService } from '../shared/event.service'
import { ActivatedRoute, Params } from '@angular/router'
import { IEvent, ISession } from '../shared/index'

@Component({
  templateUrl: '/app/events/event-details/event-details.component.html',
  styles: [`
    .container { padding-left:20px; padding-right:20px; }
    .event-image { height: 100px; }
    a {cursor: pointer}
  `]
})
export class EventDetailsComponent {
  event: IEvent;
  addMode: boolean;
  filterBy: string = 'all';
  sortBy: string = 'votes';

  constructor(private eventService: EventService, private route: ActivatedRoute) {

  }
  ngOnInit() {

    //routing a component to itself needs a complete reset of the state. To achieve this, nemove the snapshot version of this.event below and add in the forEach version

    //the following was removed because we needed to move from a params foreach to a data foreach to support using a resolver for data retrieval
    //this.route.params.forEach((params: Params) => {
    //  // the following code was removed to move from a synchronous call to publisher/subscriber call
    //  //this.event = this.eventService.getEvent(+params['id'])
    //  //this.addMode = false;

    //  // the following was removed to move the code into a resolver, which will preload before running the page (it's an attibute in the router)
    //  //this.eventService.getEvent(+params['id']).subscribe((event: IEvent) => {
    //  //  this.event = event;
    //  //  this.addMode = false;
    //  //});

    //  //the following is needed only when the data doesn't change. We no longer want to subscribe to the parameters, we're better off subscribing to the data'
    //  //this.event = this.route.snapshot.data["event"];

    //  this.addMode = false;
    //})

    //this.event = this.eventService.getEvent(+this.route.snapshot.params['id'])

    this.route.data.forEach((data) => {
      this.event = data['event'];
      this.addMode = false;
    });
  }

  addSession() {
    this.addMode = true;
  }

  saveNewSession(session: ISession)
  {
    const nextId = Math.max.apply(null, this.event.sessions.map(s => s.id));
    session.id = nextId + 1;
    this.event.sessions.push(session)
    //this.eventService.updateEvent(this.event)
    this.eventService.saveEvent(this.event).subscribe((event) => { });
    this.addMode = false;
  }

  cancelAddSession() {
    this.addMode = false
  }

}