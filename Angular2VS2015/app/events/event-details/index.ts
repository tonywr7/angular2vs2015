//export * from './event-route-activator.service' no longer used in this method
export * from './event-details.component'
export * from './create-session.component'
export * from './session-list.component'
export * from './upvote.component'
export * from './voter.service'