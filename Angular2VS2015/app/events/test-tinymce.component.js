"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var index_1 = require("./shared/index");
//import { EJ_TOKEN } from '../common/ej.service'
var jQuery_service_1 = require("../common/jQuery.service");
var TestTinyMCEComponent = (function () {
    function TestTinyMCEComponent(router, eventService, $) {
        this.router = router;
        this.eventService = eventService;
        this.$ = $;
    }
    TestTinyMCEComponent.prototype.ngOnInit = function () {
        tinymce.init({
            selector: '#tinymce',
            theme: 'inlite',
            plugins: 'image table link paste contextmenu textpattern autolink',
            insert_toolbar: 'quickimage quicktable',
            selection_toolbar: 'bold italic | quicklink h2 h3 blockquote',
            inline: true,
            paste_data_images: true,
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    };
    return TestTinyMCEComponent;
}());
TestTinyMCEComponent = __decorate([
    core_1.Component({
        templateUrl: 'app/events/test-tinymce.component.html',
        styles: ["\n    blockquote {\n      padding-left: 4px;\n      border-left: 2px solid gray;\n    }\n\n    a[href] {\n      text-decoration: underline;\n    }\n\n    .tinymce { background-color:#fff }\n  "]
    }),
    __param(2, core_1.Inject(jQuery_service_1.JQ_TOKEN)),
    __metadata("design:paramtypes", [router_1.Router, index_1.EventService, Object])
], TestTinyMCEComponent);
exports.TestTinyMCEComponent = TestTinyMCEComponent;
//# sourceMappingURL=test-tinymce.component.js.map