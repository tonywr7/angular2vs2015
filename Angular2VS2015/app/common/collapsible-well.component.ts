﻿import { Component, Input } from '@angular/core'

@Component({
  selector: 'collapsible-well',
  template: `
<div (click)="toggleContent()" class="well pointable">
  <h4><ng-content select="[well-title]" ></ng-content></h4>
  <ng-content select="[well-body]" *ngIf="visible"></ng-content>
</div>
` /* ng-content tag tells angular to put whatever is between the selector tags in here */
})
export class CollapsibleWellComponent {
  visible: boolean = true;

  toggleContent() {
    this.visible = !this.visible
  }
}