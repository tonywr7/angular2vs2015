"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var CatzListComponent = (function () {
    function CatzListComponent(route) {
        this.route = route;
    }
    CatzListComponent.prototype.ngOnInit = function () {
        this.people = this.route.snapshot.data['people'];
        this.catsByGender = [];
        //give me a unique list of genders
        var genders = _(this.people.map(function (p) { return p.gender; })).uniq().value();
        for (var i = 0; i < genders.length; i++) {
            var ownersForThisGender = _(this.people).filter({ gender: genders[i] }).value();
            var petsForThisGender = [];
            ownersForThisGender.forEach(function (p) {
                if (p.pets != null) {
                    p.pets.filter(function (pet) { return pet.type === "Cat"; }).forEach(function (pet) { return petsForThisGender.push(pet.name); });
                }
            });
            this.catsByGender.push({ gender: genders[i], pets: petsForThisGender.sort() });
        }
        //console.log(this.catsByGender);
    };
    return CatzListComponent;
}());
CatzListComponent = __decorate([
    core_1.Component({
        templateUrl: 'app/catz/catz-list.component.html',
        styles: ["\n    em {float:right; color:#E05C65; padding-left:10px;}\n    .error input {background-color:#E3C3C5;}\n    .error ::-webkit-input-placeholder { color: #999; }\n    .error :-moz-placeholder { color: #999; }\n    .error ::-moz-placeholder {color: #999; }\n    .error :ms-input-placeholder { color: #999; }\n  "]
    }),
    __metadata("design:paramtypes", [router_1.ActivatedRoute])
], CatzListComponent);
exports.CatzListComponent = CatzListComponent;
//# sourceMappingURL=catz-list.component.js.map