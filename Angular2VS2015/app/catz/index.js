"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(require("./catz.service"));
__export(require("./catz-list-resolver.service"));
__export(require("./catz-list.component"));
//# sourceMappingURL=index.js.map