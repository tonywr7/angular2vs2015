"use strict";
var _404_component_1 = require("./errors/404.component");
var index_1 = require("./catz/index");
var index_2 = require("./events/index");
exports.appRoutes = [
    { path: 'catz', component: index_1.CatzListComponent, resolve: { people: index_1.CatzListResolver } },
    { path: 'events', component: index_2.EventsListComponent, resolve: { events: index_2.EventListResolver } },
    { path: 'events/new', component: index_2.CreateEventComponent, canDeactivate: ['canDeactivateCreateEvent'] },
    { path: 'events/testgrid', component: index_2.TestGridEventComponent },
    { path: 'events/testsignalr', component: index_2.TestSignalRComponent },
    { path: 'events/testtinymce', component: index_2.TestTinyMCEComponent },
    // { path: 'events/:id', component: EventDetailsComponent, canActivate: [EventRouteActivator]}, route activator no longer used, changed to resolve to preload data *S97*
    { path: 'events/:id', component: index_2.EventDetailsComponent, resolve: { event: index_2.EventResolver } },
    { path: 'events/session/new', component: index_2.CreateSessionComponent },
    { path: '404', component: _404_component_1.Error404Component },
    { path: '', redirectTo: '/events', pathMatch: 'full' },
    { path: 'user', loadChildren: 'app/user/user.module#UserModule' }
];
//# sourceMappingURL=routes.js.map