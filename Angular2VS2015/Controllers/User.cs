﻿namespace Angular2VS2015.Controllers
{
  public class User
  {
    public int id { get; set; }
    public string firstName { get; set; }
    public string lastName { get; set; }
    public string userName { get; set; }
  }
}