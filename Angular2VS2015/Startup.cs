﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Angular2VS2015.Startup))]

namespace Angular2VS2015
{
  public class Startup
  {
    public void Configuration(IAppBuilder app)
    {
      app.MapSignalR();
      app.AngularServiceJavascriptGenerator();
    }
  }
}
