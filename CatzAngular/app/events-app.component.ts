import { Component } from '@angular/core'
import { AuthService } from './user/auth.service'

// ***7***

@Component({
    selector: 'events-app',
    //template: `
    //<nav-bar></nav-bar>
    //<events-list></events-list> <!-- ***8*** -->
    //`
    //template: `
    //<nav-bar></nav-bar>
    //<router-outlet></router-outlet> <!-- Change from core component to router outlet instead! -->
    //`
    template: `
    <router-outlet></router-outlet> <!-- Change from core component to router outlet instead! -->
    `
})
export class EventsAppComponent
{
  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.auth.checkAuthenticationStatus();
  }
}