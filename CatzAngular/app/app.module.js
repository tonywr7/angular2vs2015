"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require("@angular/router");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var ej_angular2_1 = require("ej-angular2");
var index_1 = require("./catz/index");
var index_2 = require("./events/index");
var events_app_component_1 = require("./events-app.component");
var navbar_component_1 = require("./nav/navbar.component");
//import { ToastrService } from './common/toastr.service'
var index_3 = require("./common/index");
var routes_1 = require("./routes");
var _404_component_1 = require("./errors/404.component");
var auth_service_1 = require("./user/auth.service");
// ***5***
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            ej_angular2_1.EJAngular2Module.forRoot(),
            http_1.JsonpModule,
            forms_1.ReactiveFormsModule,
            router_1.RouterModule.forRoot(routes_1.appRoutes)
        ],
        declarations: [
            /* components */
            index_1.CatzListComponent,
            events_app_component_1.EventsAppComponent,
            index_2.EventsListComponent,
            index_2.EventThumbnailComponent,
            index_2.EventDetailsComponent,
            index_2.CreateEventComponent,
            _404_component_1.Error404Component,
            navbar_component_1.NavBarComponent,
            index_2.CreateSessionComponent,
            index_2.SessionListComponent,
            index_3.CollapsibleWellComponent,
            index_3.SimpleModalComponent,
            index_2.UpvoteComponent,
            index_2.TestGridEventComponent,
            index_2.TestSignalRComponent,
            index_2.TestTinyMCEComponent,
            /* pipes */
            index_2.DurationPipe,
            /* directives */
            index_3.ModalTriggerDirective,
            index_2.LocationValidatorDirective
        ],
        providers: [
            index_1.CatzService,
            index_1.CatzListResolver,
            index_2.EventService,
            //ToastrService,
            { provide: index_3.TOASTR_TOKEN, useValue: toastr },
            { provide: index_3.JQ_TOKEN, useValue: jQuery },
            //EventRouteActivator, this was removed as it is no longer being used in this method *S97*
            { provide: 'canDeactivateCreateEvent', useValue: checkDirtyState },
            index_2.EventListResolver,
            index_2.EventResolver,
            auth_service_1.AuthService,
            index_2.VoterService
        ],
        bootstrap: [events_app_component_1.EventsAppComponent] /* this is the start component ***6*** */
    })
], AppModule);
exports.AppModule = AppModule;
function checkDirtyState(component) {
    if (component.isDirty) {
        return window.confirm('You have not saved this event, do you really want to cancel?');
    }
    return false;
}
//# sourceMappingURL=app.module.js.map