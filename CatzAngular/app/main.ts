import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import { AppModule } from './app.module';

// ***4***
platformBrowserDynamic().bootstrapModule(AppModule);