import { Component, Inject, ElementRef, ViewChild } from '@angular/core'
import { Router } from '@angular/router'
import { EventService } from './shared/index'
//import { EJ_TOKEN } from '../common/ej.service'
import { JQ_TOKEN } from '../common/jQuery.service'

declare let ej: any;

@Component({
  templateUrl: 'app/events/test-signalr.component.html'
})
export class TestSignalRComponent {

  @ViewChild('mygrid')
  public mygrid: ElementRef;

  public gridData: any;
  public pagesize: number = 5;

  @ViewChild("name")
  public name: ElementRef;
  @ViewChild("message")
  public message: ElementRef;
  public messages: string[] = ["First message", "Second message"];
  public chatHub: any = null;

  constructor(private router: Router, private eventService: EventService, @Inject(JQ_TOKEN) private $: any) {

  }

  ngOnInit() {

    this.name.nativeElement.value = 'Guest';
    this.message.nativeElement.value = 'Default message';

    this.chatHub = this.$.connection.chatHub;
    this.chatHub.client.broadcastMessage = (name, message) => {
      var newMessage = name + ' says: ' + message;
      console.log("got message");
      // push the newly coming message to the collection of messages
      this.messages.push(newMessage);
      //this.$apply();
    };
    this.$.connection.hub.start();



    this.gridData = [{
      OrderID: 10248, CustomerID: 'VINET', EmployeeID: 5,
      OrderDate: new Date(8364186e5), Freight: 32.38
    },
    {
      OrderID: 10249, CustomerID: 'TOMSP', EmployeeID: 6,
      OrderDate: new Date(836505e6), Freight: 11.61
    },
    {
      OrderID: 10250, CustomerID: 'HANAR', EmployeeID: 4,
      OrderDate: new Date(8367642e5), Freight: 65.83
    },
    {
      OrderID: 10251, CustomerID: 'VICTE', EmployeeID: 3,
      OrderDate: new Date(8367642e5), Freight: 41.34
    },
    {
      OrderID: 10252, CustomerID: 'SUPRD', EmployeeID: 4,
      OrderDate: new Date(8368506e5), Freight: 51.3
    },
    {
      OrderID: 20248, CustomerID: 'AINET', EmployeeID: 5,
      OrderDate: new Date(8364186e5), Freight: 132.38
    },
    {
      OrderID: 20249, CustomerID: 'AOMSP', EmployeeID: 6,
      OrderDate: new Date(836505e6), Freight: 111.61
    },
    {
      OrderID: 20250, CustomerID: 'AANAR', EmployeeID: 4,
      OrderDate: new Date(8367642e5), Freight: 165.83
    },
    {
      OrderID: 20251, CustomerID: 'AICTE', EmployeeID: 3,
      OrderDate: new Date(8367642e5), Freight: 141.34
    },
    {
      OrderID: 20252, CustomerID: 'AUPRD', EmployeeID: 4,
      OrderDate: new Date(8368506e5), Freight: 151.3
    }];

    //var data = ej.DataManager(window["gridData"]).executeLocal(ej.Query().take(50));

    //this.$("#Grid").ejGrid({
    //  dataSource: data,
    //  allowPaging: true,
    //  columns: [
    //    { field: "OrderID", headerText: "Order ID", width: 75, textAlign: ej.TextAlign.Right },
    //    { field: "CustomerID", headerText: "Customer ID", width: 80 },
    //    { field: "EmployeeID", headerText: "Employee ID", width: 75, textAlign: ej.TextAlign.Right },
    //    { field: "Freight", width: 75, format: "{0:C}", textAlign: ej.TextAlign.Right },
    //    { field: "OrderDate", headerText: "Order Date", width: 80, format: "{0:MM/dd/yyyy}", textAlign: ej.TextAlign.Right },
    //    { field: "ShipCity", headerText: "Ship City", width: 110 }
    //  ]
    //});     

  }

  newMessage() {

    // sends a new message to the server
    this.chatHub.server.sendMessage(this.name.nativeElement.value, this.message.nativeElement.value);
    console.log("Sent message");
    this.message.nativeElement.value = '';
  }

  myclick() {
    console.log(this);
    console.log(this.$("#mygrid"))
    //console.log(this.mygrid.el.nativeElement)
  }

}