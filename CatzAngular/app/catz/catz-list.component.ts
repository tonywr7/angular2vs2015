﻿import { Directive, ElementRef, Inject, Input, OnInit } from '@angular/core'
import { Component } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { IPetOwner, IPet, CatsByGender } from './index'
//import * as _ from 'lodash';

declare let _: any;

@Component({
  templateUrl: 'app/catz/catz-list.component.html',
  styles: [`
    em {float:right; color:#E05C65; padding-left:10px;}
    .error input {background-color:#E3C3C5;}
    .error ::-webkit-input-placeholder { color: #999; }
    .error :-moz-placeholder { color: #999; }
    .error ::-moz-placeholder {color: #999; }
    .error :ms-input-placeholder { color: #999; }
  `]
})
export class CatzListComponent implements OnInit {

  private people: IPetOwner[];

  public catsByGender: CatsByGender[];

  constructor(private route: ActivatedRoute) {

  }

  ngOnInit() {

    this.people = this.route.snapshot.data['people']

    this.catsByGender = [];

    //give me a unique list of genders
    var genders = _(this.people.map((p) => p.gender)).uniq().value();
    for (var i = 0; i < genders.length; i++) {
      var ownersForThisGender = _(this.people).filter({ gender: genders[i] }).value();
      var petsForThisGender = [];
      ownersForThisGender.forEach((p) => {
        if (p.pets != null) {
          p.pets.filter((pet) => pet.type === "Cat").forEach((pet) => petsForThisGender.push(pet.name));
        }
      });
      this.catsByGender.push({ gender: genders[i], pets: petsForThisGender.sort() });
    }
    //console.log(this.catsByGender);

  }

}