﻿import { Injectable, EventEmitter } from '@angular/core'
import { Subject, Observable } from 'rxjs/RX'
import { IPetOwner, IPet } from './index'
import { Http, Response, Headers, RequestOptions, Jsonp } from '@angular/http'

@Injectable()
export class CatzService {

  constructor(private http: Http, private jsonp: Jsonp) { }

  getCatz(): Observable<IPetOwner[]> {

    return this.jsonp.get("http://agl-developer-test.azurewebsites.net/people.json?callback=JSONP_CALLBACK").map( (response: Response)=>{
      return <IPetOwner[]>response.json();
    }).catch(this.handleError);

  }

  private handleError(error: Response)
  {
    return Observable.throw(error.statusText);
  }

}
