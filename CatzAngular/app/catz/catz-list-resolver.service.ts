﻿import { Injectable } from '@angular/core'
import { Resolve } from '@angular/router'
import { CatzService } from './index'

@Injectable()
export class CatzListResolver implements Resolve<any> {
  constructor(private catzService:CatzService) {

  }

  resolve() {
    return this.catzService.getCatz().map(catz => catz)
  }
}