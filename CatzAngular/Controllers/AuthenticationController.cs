﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Authentication;
using System.Web;
using System.Web.Http;

namespace Angular2VS2015.Controllers
{
  [RoutePrefix("api/auth")]
  public class AuthenticationController : ApiController
  {
    [HttpPost, Route("login")]
    public User Login(LoginInfo info)
    {
      if (info.username == "tone")
      {
        var newUser = new User { id = 98, firstName = "Tony", lastName = "Wright", userName = "tone" };
        HttpContext.Current.Session["auth"] = newUser;
        return newUser;
      }
      return null;
    }

    [HttpGet, Route("currentIdentity")]
    public User CurrentIdentity()
    {
      //returns null if not authenticated, otherwise returns the current user.
      var currentUser = HttpContext.Current.Session["auth"] as User;
      return currentUser;
    }

    [HttpPost, Route("updateCurrentUser")]
    public HttpResponseMessage UpdateCurrentUser(User currentUser)
    {
      HttpContext.Current.Session["auth"] = currentUser;
      var response = new HttpResponseMessage();
      response.Headers.Add("Message", "Successfully updated!");
      return response;
    }

    [HttpPost, Route("logout")]
    public bool Logout()
    {
      HttpContext.Current.Session.Remove("auth");
      return true;
    }

  }
}
